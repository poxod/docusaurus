/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'Faisal Abdi',
  tagline: 'My IT Notebook',
  url: 'https://poxod.co.uk',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'facebook', // Usually your GitHub org/user name.
  projectName: 'docusaurus', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'My Site',
      logo: {
        alt: 'My Site Logo',
        src: 'img/logo.svg',
      },
      items: [
        {
          to: 'docs/',
          activeBasePath: 'docs',
          label: 'Docs',
          position: 'left',
        },
        {to: 'blog', label: 'Blog', position: 'left'},
        {
          href: 'https://github.com/facebook/docusaurus',
          label: 'GitHub',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Docs',
          items: [
            {
              label: 'Getting Started',
              to: 'docs/',
            },
          ],
        },
        
        {
          title: 'More',
          items: [
            {
              label: 'LinkedIn',
              href: 'https://www.linkedin.com/in/faisal-abdi-32347b168/',
            },
            {
              label: 'GitHub',
              href: 'https://github.com/fabdi00?tab=repositories',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} Faisal Abdi. Built with Docusaurus.`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl:
            'https://gitlab.com/poxod/docusaurus/poxod/docs/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://gitlab.com/poxod/docusaurus/-/tree/master/poxod/blog/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
